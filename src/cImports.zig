const std = @import("std");

pub const c = @cImport({
    @cInclude("linux/gpio.h");
    @cInclude("linux/spi/spidev.h");
    @cInclude("string.h");
});
