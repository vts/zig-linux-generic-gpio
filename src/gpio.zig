const std = @import("std");
const utils = @import("./utils.zig");
const c = @import("./cImports.zig").c;

const log = std.log.scoped(.linuxGenericGpio_gpio);

const DEVICE_PATH_PREFIX = "/dev/gpiochip";
const DEVICE_PATH_MAX_SIZE = DEVICE_PATH_PREFIX.len + 20;

const ArrayWithSliceDevicePath = utils.ArrayWithSlice(u8, DEVICE_PATH_MAX_SIZE);
const ArrayWithSliceGpioName = utils.ArrayWithSlice(u8, c.GPIO_MAX_NAME_SIZE);

pub const GPIOChip = struct {
    handle: std.os.fd_t,
    mutex: std.Thread.Mutex,
    // This is the name that is registered with the kernel for all of the
    // GPIO Claims.
    programLabel: []const u8,

    devicePath: ArrayWithSliceDevicePath,
    chipName: ArrayWithSliceGpioName,
    label: ArrayWithSliceGpioName,
    numLines: u32,
    activePins: [c.GPIO_V2_LINES_MAX]?ActivePin,

    pub fn open(self: *GPIOChip, device_id: u32, programLabel: []const u8) !void {
        // Clear the gpioChip.
        self.* = std.mem.zeroes(GPIOChip);

        self.mutex = std.Thread.Mutex{};

        var deviceBuffer: [DEVICE_PATH_MAX_SIZE]u8 = undefined;
        @memset(&deviceBuffer, 0);

        const devicePath = try std.fmt.bufPrint(&deviceBuffer, "{s}{d}", .{ DEVICE_PATH_PREFIX, device_id });

        log.debug("Checking the string: {s}", .{devicePath});

        const handle = try std.os.open(devicePath, std.os.O.RDWR | std.os.O.CLOEXEC, 0);
        if (handle < 0) {
            return error.OSError;
        }
        errdefer std.os.close(handle);

        var gpioInfo: c.gpiochip_info = undefined;

        if (std.os.linux.ioctl(handle, c.GPIO_GET_CHIPINFO_IOCTL, @intFromPtr(&gpioInfo)) != 0) {
            return error.OSError;
        }

        self.handle = handle;
        ArrayWithSliceGpioName.init(&self.chipName, gpioInfo.name, c.strnlen(&gpioInfo.name, c.GPIO_MAX_NAME_SIZE));
        ArrayWithSliceGpioName.init(&self.label, gpioInfo.label, c.strnlen(&gpioInfo.label, c.GPIO_MAX_NAME_SIZE));
        ArrayWithSliceDevicePath.init(&self.devicePath, deviceBuffer, devicePath.len);
        self.programLabel = programLabel;

        log.debug("Chip Name: {s}, label: {s}", .{ self.chipName, self.label });
    }

    pub fn deinit(self: *GPIOChip) void {
        if (self.handle > 0) {
            std.os.close(self.handle);
            self.handle = -1;
        }
    }

    pub fn configurePins(self: *GPIOChip, configuration: Configuration) !void {
        var lineRequest: c.gpio_v2_line_request = undefined;

        // TODO: If a pin is in active pins, should release or ignore it.
        // maybe just set the value?

        try configuration.initLineRequest(&lineRequest, self.programLabel);

        self.mutex.lock();
        defer self.mutex.unlock();

        log.debug("pre - ioctl", .{});

        if (std.os.linux.ioctl(self.handle, c.GPIO_V2_GET_LINE_IOCTL, @intFromPtr(&lineRequest)) != 0) {
            return error.OSError;
        }
        log.debug("post - ioctl", .{});

        if (lineRequest.fd == 0) {
            return error.OSError;
        }

        for (configuration.pins, 0..) |pin, offset| {
            log.debug("Active Pins: {d} -> {d} -> {d}", .{ pin.pin, lineRequest.fd, offset });
            self.activePins[pin.pin] = .{
                .fd = lineRequest.fd,
                .offset = @intCast(u6, offset),
            };
        }
    }

    pub fn setPinValue(self: *GPIOChip, pin: u6, value: bool) !void {
        var lineValues: c.gpio_v2_line_values = std.mem.zeroes(c.gpio_v2_line_values);

        const maybeActivePin = self.activePins[pin];
        if (maybeActivePin) |activePin| {
            const bit = @as(u64, 1) << activePin.offset;
            lineValues.mask = bit;
            if (value) {
                lineValues.bits = bit;
            }

            self.mutex.lock();
            defer self.mutex.unlock();

            if (std.os.linux.ioctl(activePin.fd, c.GPIO_V2_LINE_SET_VALUES_IOCTL, @intFromPtr(&lineValues)) != 0) {
                return error.OSError;
            }
        } else {
            return error.InactivePin;
        }
    }

    pub fn getPinValue(self: *GPIOChip, pin: u6) !bool {
        var lineValues: c.gpio_v2_line_values = std.mem.zeroes(c.gpio_v2_line_values);

        const maybeActivePin = self.activePins[pin];
        if (maybeActivePin) |activePin| {
            const bit = @as(u64, 1) << @intCast(u6, activePin.offset);
            lineValues.mask = bit;

            self.mutex.lock();
            defer self.mutex.unlock();

            if (std.os.linux.ioctl(activePin.fd, c.GPIO_V2_LINE_SET_VALUES_IOCTL, @intFromPtr(&lineValues)) != 0) {
                return error.OSError;
            }

            return lineValues.bits == bit;
        } else {
            return error.InactivePin;
        }
    }

    pub const Flag = enum {
        risingEdge,
        fallingEdge,
        activeLow,
        openDrain,
        openSource,
        pullUp,
        pullDown,
        pullNone,
        input,
        output,

        fn createKernelFlags(flags: []const Flag) u64 {
            var result: u64 = 0;

            for (flags) |flag| {
                result |= switch (flag) {
                    Flag.risingEdge => c.GPIO_V2_LINE_FLAG_EDGE_RISING,
                    Flag.fallingEdge => c.GPIO_V2_LINE_FLAG_EDGE_FALLING,
                    Flag.activeLow => c.GPIO_V2_LINE_FLAG_ACTIVE_LOW,
                    Flag.openDrain => c.GPIO_V2_LINE_FLAG_OPEN_DRAIN,
                    Flag.openSource => c.GPIO_V2_LINE_FLAG_OPEN_SOURCE,
                    Flag.pullUp => c.GPIO_V2_LINE_FLAG_BIAS_PULL_UP,
                    Flag.pullDown => c.GPIO_V2_LINE_FLAG_BIAS_PULL_DOWN,
                    Flag.pullNone => c.GPIO_V2_LINE_FLAG_BIAS_DISABLED,
                    Flag.input => c.GPIO_V2_LINE_FLAG_INPUT,
                    Flag.output => c.GPIO_V2_LINE_FLAG_OUTPUT,
                };
            }

            return result;
        }

        test "createKernelFlags: Empty flags returns zero" {
            try std.testing.expectEqual(createKernelFlags(&.{}), 0);
        }

        test "createKernelFlags: Check each of the fields individually" {
            try std.testing.expectEqual(createKernelFlags(&.{Flag.risingEdge}), c.GPIO_V2_LINE_FLAG_EDGE_RISING);
            try std.testing.expectEqual(createKernelFlags(&.{Flag.fallingEdge}), c.GPIO_V2_LINE_FLAG_EDGE_FALLING);
            try std.testing.expectEqual(createKernelFlags(&.{Flag.activeLow}), c.GPIO_V2_LINE_FLAG_ACTIVE_LOW);
            try std.testing.expectEqual(createKernelFlags(&.{Flag.openDrain}), c.GPIO_V2_LINE_FLAG_OPEN_DRAIN);
            try std.testing.expectEqual(createKernelFlags(&.{Flag.openSource}), c.GPIO_V2_LINE_FLAG_OPEN_SOURCE);
            try std.testing.expectEqual(createKernelFlags(&.{Flag.pullUp}), c.GPIO_V2_LINE_FLAG_BIAS_PULL_UP);
            try std.testing.expectEqual(createKernelFlags(&.{Flag.pullDown}), c.GPIO_V2_LINE_FLAG_BIAS_PULL_DOWN);
            try std.testing.expectEqual(createKernelFlags(&.{Flag.pullNone}), c.GPIO_V2_LINE_FLAG_BIAS_DISABLED);
            try std.testing.expectEqual(createKernelFlags(&.{Flag.input}), c.GPIO_V2_LINE_FLAG_INPUT);
            try std.testing.expectEqual(createKernelFlags(&.{Flag.output}), c.GPIO_V2_LINE_FLAG_OUTPUT);
        }

        test "createKernelFlags: Check simple combinations for flags" {
            // These are probably invalid combinations of flags,
            // just here to confirm that the or operation is working
            // properly.
            try std.testing.expectEqual(createKernelFlags(&.{ Flag.risingEdge, Flag.input }), c.GPIO_V2_LINE_FLAG_EDGE_RISING | c.GPIO_V2_LINE_FLAG_INPUT);

            try std.testing.expectEqual(createKernelFlags(&.{ Flag.fallingEdge, Flag.pullUp, Flag.output }), c.GPIO_V2_LINE_FLAG_EDGE_FALLING | c.GPIO_V2_LINE_FLAG_BIAS_PULL_UP | c.GPIO_V2_LINE_FLAG_OUTPUT);
        }
    };

    pub const Configuration = struct {
        flags: []const Flag,
        pins: []const PinConfiguration,

        pub const PinConfiguration = struct {
            pin: u6,
            value: ?bool,
        };

        fn initLineRequest(self: Configuration, lineRequest: *c.gpio_v2_line_request, programLabel: []const u8) !void {
            if (self.pins.len == 0) {
                return error.InvalidArgument;
            }

            if (self.flags.len == 0) {
                return error.InvalidArgument;
            }

            lineRequest.* = std.mem.zeroes(c.gpio_v2_line_request);

            for (self.pins, 0..) |pinConfiguration, index| {
                lineRequest.offsets[index] = pinConfiguration.pin;
            }
            lineRequest.num_lines = @intCast(c_uint, self.pins.len);

            std.mem.copy(u8, &lineRequest.consumer, programLabel);

            lineRequest.config.flags = Flag.createKernelFlags(self.flags);

            if (createValueAttributes(self.pins)) |valuesAttribute| {
                lineRequest.config.attrs[0] = valuesAttribute;
                lineRequest.config.num_attrs = 1;
            }
        }

        fn createValueAttributes(pins: []const PinConfiguration) ?c.gpio_v2_line_config_attribute {
            var mask: u64 = 0;
            var values: u64 = 0;

            for (pins, 0..) |pin, index| {
                if (pin.value) |value| {
                    const bit = @as(u64, 1) << @intCast(u6, index);
                    mask |= bit;
                    if (value) {
                        values |= bit;
                    }
                }
            }

            if (mask == 0) {
                return null;
            }

            return c.gpio_v2_line_config_attribute{
                .attr = .{ .id = c.GPIO_V2_LINE_ATTR_ID_OUTPUT_VALUES, .padding = 0, .unnamed_0 = .{
                    .values = values,
                } },
                .mask = mask,
            };
        }

        test "initLineRequest: Zero pins should return an error" {
            const configuration = Configuration{
                .flags = &[_]Flag{Flag.input},
                .pins = &[_]PinConfiguration{},
            };

            var lineRequest: c.gpio_v2_line_request = undefined;

            try std.testing.expectError(error.InvalidArgument, configuration.initLineRequest(&lineRequest, "test-app"));
        }

        test "initLineRequest: Zero flags should return an error" {
            const configuration = Configuration{
                .flags = &[_]Flag{},
                .pins = &[_]PinConfiguration{},
            };

            var lineRequest: c.gpio_v2_line_request = undefined;

            try std.testing.expectError(error.InvalidArgument, configuration.initLineRequest(&lineRequest, "test-app"));
        }

        test "initLineRequest: A single pin should return a valid configuration" {
            const expected = c.gpio_v2_line_request{
                .offsets = utils.expandArray(u32, c.GPIO_V2_LINES_MAX, &.{12}),
                .consumer = utils.expandArray(u8, c.GPIO_MAX_NAME_SIZE, "test-app"),
                .config = c.gpio_v2_line_config{
                    .flags = c.GPIO_V2_LINE_FLAG_INPUT,
                    .num_attrs = 0,
                    .padding = [_]u32{0} ** 5,
                    // TODO: Convert this to zero.
                    .attrs = [_]c.gpio_v2_line_config_attribute{.{
                        .attr = .{
                            .id = 0,
                            .padding = 0,
                            .unnamed_0 = .{
                                .values = 0,
                            },
                        },
                        .mask = 0,
                    }} ** c.GPIO_V2_LINE_NUM_ATTRS_MAX,
                },
                .num_lines = 1,
                .event_buffer_size = 0,
                .padding = [_]u32{0} ** 5,
                .fd = 0,
            };

            const configuration = Configuration{
                .flags = &[_]Flag{Flag.input},
                .pins = &[_]PinConfiguration{.{
                    .pin = 12,
                    .value = null,
                }},
            };

            var lineRequest: c.gpio_v2_line_request = undefined;

            try configuration.initLineRequest(&lineRequest, "test-app");
            try TestHelpers.compareLineRequest(&expected, &lineRequest);
        }

        test "initLineRequest: A single pin with values should return a valid configuration" {
            var expected = c.gpio_v2_line_request{
                .offsets = utils.expandArray(u32, c.GPIO_V2_LINES_MAX, &.{12}),
                .consumer = utils.expandArray(u8, c.GPIO_MAX_NAME_SIZE, "test-app"),
                .config = std.mem.zeroes(c.gpio_v2_line_config),
                .num_lines = 1,
                .event_buffer_size = 0,
                .padding = [_]u32{0} ** 5,
                .fd = 0,
            };

            expected.config.flags = c.GPIO_V2_LINE_FLAG_OUTPUT;
            expected.config.num_attrs = 1;
            expected.config.attrs[0] = .{
                .attr = .{
                    .id = c.GPIO_V2_LINE_ATTR_ID_OUTPUT_VALUES,
                    .padding = 0,
                    .unnamed_0 = .{
                        .values = 1,
                    },
                },
                .mask = 1,
            };

            const configuration = Configuration{
                .flags = &[_]Flag{Flag.output},
                .pins = &[_]PinConfiguration{.{
                    .pin = 12,
                    .value = true,
                }},
            };

            var lineRequest: c.gpio_v2_line_request = undefined;

            try configuration.initLineRequest(&lineRequest, "test-app");
            try TestHelpers.compareLineRequest(&expected, &lineRequest);
        }

        test "createValueAttribues: A pin configuration without any values does not create an attribute" {
            const pins = &[_]PinConfiguration{.{
                .pin = 12,
                .value = null,
            }};

            try std.testing.expect(createValueAttributes(pins) == null);
        }

        test "createValueAttributes: A pin configuration with values should create an attribute" {
            const pins = &[_]PinConfiguration{ .{
                .pin = 12,
                .value = true,
            }, .{
                .pin = 14,
                .value = null,
            }, .{
                .pin = 18,
                .value = false,
            } };

            try TestHelpers.compareLineConfigAttribute(&c.gpio_v2_line_config_attribute{
                .attr = .{ .id = c.GPIO_V2_LINE_ATTR_ID_OUTPUT_VALUES, .padding = 0, .unnamed_0 = .{
                    .values = 0b001,
                } },
                .mask = 0b101,
            }, &createValueAttributes(pins).?);
        }
    };

    const ActivePin = struct {
        fd: i32,
        offset: u6,
    };
};

const TestHelpers = struct {
    const AttributeEnum = enum {
        unset,
        flags,
        values,
        debounce_period_us,
    };

    const GPIOV2LineAttribute = union(AttributeEnum) {
        unset: void,
        flags: u64,
        values: u64,
        debounce_period_us: u32,
    };

    const GPIOV2LineConfigAttribute = struct {
        attr: GPIOV2LineAttribute,
        mask: u64,

        fn mirror(self: *GPIOV2LineConfigAttribute, lineConfigAttribute: *const c.gpio_v2_line_config_attribute) void {
            self.mask = lineConfigAttribute.mask;
            self.attr = switch (lineConfigAttribute.attr.id) {
                c.GPIO_V2_LINE_ATTR_ID_FLAGS => .{ .flags = lineConfigAttribute.attr.unnamed_0.flags },
                c.GPIO_V2_LINE_ATTR_ID_OUTPUT_VALUES => .{ .values = lineConfigAttribute.attr.unnamed_0.values },
                c.GPIO_V2_LINE_ATTR_ID_DEBOUNCE => .{ .debounce_period_us = lineConfigAttribute.attr.unnamed_0.debounce_period_us },
                else => .{ .unset = undefined },
            };
        }
    };

    const GPIOV2LineConfig = struct {
        flags: u64,
        num_attrs: u32,
        attrs: [10]GPIOV2LineConfigAttribute,

        fn mirror(self: *GPIOV2LineConfig, config: *const c.gpio_v2_line_config) void {
            self.flags = config.flags;
            self.num_attrs = config.num_attrs;
            for (&self.attrs, 0..) |*selfAttr, index| {
                selfAttr.mirror(&config.attrs[index]);
            }
        }
    };

    const GPIOV2LineRequest = struct {
        offsets: [64]u32,
        consumer: [32]u8,
        config: GPIOV2LineConfig,
        num_lines: u32,
        event_buffer_size: u32,
        fd: c_int,

        fn mirror(self: *GPIOV2LineRequest, lineRequest: *const c.gpio_v2_line_request) void {
            self.offsets = lineRequest.offsets;
            self.consumer = lineRequest.consumer;
            self.num_lines = lineRequest.num_lines;
            self.config.mirror(&lineRequest.config);
            self.event_buffer_size = lineRequest.event_buffer_size;
            self.fd = lineRequest.fd;
        }
    };

    fn compareLineRequest(expected: *const c.gpio_v2_line_request, actual: *const c.gpio_v2_line_request) !void {
        var expectedMirror: GPIOV2LineRequest = undefined;
        var actualMirror: GPIOV2LineRequest = undefined;

        expectedMirror.mirror(expected);
        actualMirror.mirror(actual);

        try std.testing.expectEqual(expectedMirror, actualMirror);
    }

    fn compareLineConfigAttribute(expected: *const c.gpio_v2_line_config_attribute, actual: *const c.gpio_v2_line_config_attribute) !void {
        var expectedMirror: GPIOV2LineConfigAttribute = undefined;
        var actualMirror: GPIOV2LineConfigAttribute = undefined;

        expectedMirror.mirror(expected);
        actualMirror.mirror(actual);

        try std.testing.expectEqual(expectedMirror, actualMirror);
    }
};

test {
    std.testing.refAllDeclsRecursive(GPIOChip);
}
