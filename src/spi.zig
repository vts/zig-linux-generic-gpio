const std = @import("std");
const c = @import("cImports.zig").c;
const utils = @import("./utils.zig");

const DEVICE_PATH_PREFIX = "/dev/spidev";
const DEVICE_PATH_MAX_SIZE = DEVICE_PATH_PREFIX.len + 20;

// There is a bug where the Linux Constants don't compile
const CLOCK_PHASE_BIT_MASK: u8 = 0b00000001; // SPI_CPHA
const CLOCK_POLARITY_BIT_MASK: u8 = 0x00000010; // SPI_CPOL

const ArrayWithSliceDevicePath = utils.ArrayWithSlice(u8, DEVICE_PATH_MAX_SIZE);

pub const spi_ioc_transfer = c.spi_ioc_transfer;

fn SPIFactory(comptime dependencies: anytype) type {
    const DependencyState = if (@hasDecl(dependencies, "createState"))
        @TypeOf(dependencies.createState())
    else
        void;

    return struct {
        configuration: Configuration = Configuration{},
        handle: c_int = -1,
        devicePath: ArrayWithSliceDevicePath = std.mem.zeroes(ArrayWithSliceDevicePath),
        mutex: std.Thread.Mutex = std.Thread.Mutex{},
        dependencyState: DependencyState = if (@hasDecl(dependencies, "createState"))
            dependencies.createState()
        else
            undefined,

        const Self = @This();

        pub const BitJustification = enum {
            leastSignificantBitFirst,
            mostSignificantBitFirst,
        };

        pub const Mode = struct {
            clockPolarity: bool = false,
            clockPhase: bool = false,

            fn parseBits(modeBits: u8) Mode {
                return .{
                    .clockPhase = modeBits & CLOCK_PHASE_BIT_MASK != 0,
                    .clockPolarity = modeBits & CLOCK_POLARITY_BIT_MASK != 0,
                };
            }

            fn toBits(self: Mode) u8 {
                return (if (self.clockPhase) CLOCK_PHASE_BIT_MASK else 0) | (if (self.clockPolarity) CLOCK_POLARITY_BIT_MASK else 0);
            }
        };

        pub const Configuration = struct {
            // Default to 1 MHZ
            speed: u32 = 1000 * 1000,
            bitJustification: BitJustification = .mostSignificantBitFirst,
            bitsPerWord: u8 = 8,
            mode: Mode = .{},
        };

        pub const TransferData = union(enum) {
            transmit: []const u8,
            receive: []u8,
            duplex: struct {
                transmit: []const u8,
                receive: []u8,
            },
        };

        pub const Transfer = struct {
            data: TransferData,
            // Set to true to disable changing chipselect between
            deselectAfterTransfer: bool = false,
        };

        pub fn open(self: *Self, device_id: u32, channel: u32, configuration: Configuration) !void {
            self.mutex = std.Thread.Mutex{};

            self.devicePath.slice = try std.fmt.bufPrint(&self.devicePath.buffer, "{s}{d}.{d}", .{ DEVICE_PATH_PREFIX, device_id, channel });

            const handle = try inject(dependencies.open, &self.dependencyState, .{ self.devicePath.slice, std.os.O.RDWR | std.os.O.CLOEXEC, 0 });

            if (handle < 0) {
                return error.OSError;
            }
            errdefer inject(dependencies.close, &self.dependencyState, .{handle});

            try _setSpeed(&self.dependencyState, handle, configuration.speed);
            try _setMode(&self.dependencyState, handle, configuration.mode);
            try _setBitsPerWord(&self.dependencyState, handle, configuration.bitsPerWord);
            try _setBitJustification(&self.dependencyState, handle, configuration.bitJustification);

            self.handle = handle;
            self.configuration = configuration;
        }

        pub fn deinit(self: *Self) void {
            if (self.handle >= 0) {
                std.os.close(self.handle);
                self.handle = -1;
            }
        }

        pub fn transfer(self: *Self, transfers: []Transfer, messages: []c.spi_ioc_transfer) !usize {
            if (transfers.len == 0) {
                return 0;
            }

            // This will throw an error if there are too many transfers at once.
            const request = try spiIOCRequest(transfers.len);

            _ = .{ request, self };

            if (messages.len < transfers.len) {
                return error.InvalidArgument;
            }

            @memset(messages[0..transfers.len], std.mem.zeroes(c.spi_ioc_transfer));
            // Not sure of a faster way of getting around the tuple comptime usize casting in the
            // switch statement.
            var zero: usize = 0;

            for (transfers, 0..) |item, index| {
                const messageBufferInfo = switch (item.data) {
                    .transmit => |transmit| .{ @intFromPtr(transmit.ptr), zero, transmit.len },
                    .receive => |receive| .{ zero, @intFromPtr(receive.ptr), receive.len },
                    // TODO: Should it be an error if the buffers are not of equal length
                    .duplex => |buffers| .{
                        @intFromPtr(buffers.transmit.ptr),
                        @intFromPtr(buffers.receive.ptr),
                        @min(buffers.receive.len, buffers.transmit.len),
                    },
                };

                const message = &messages[index];
                message.tx_buf = messageBufferInfo[0];
                message.rx_buf = messageBufferInfo[1];
                message.len = @truncate(u32, messageBufferInfo[2]);
                message.speed_hz = self.configuration.speed;
                message.cs_change = if (item.deselectAfterTransfer) 1 else 0;
                message.bits_per_word = self.configuration.bitsPerWord;
            }

            return try _ioctl(&self.dependencyState, self.handle, request, @intFromPtr(messages.ptr));
        }

        pub fn setSpeed(self: *Self, speed: u32) !void {
            try _setSpeed(&self.dependencyState, self.handle, speed);
            self.configuration.speed = speed;
        }

        pub fn refreshSpeed(self: *Self) !u32 {
            var speed: u32 = undefined;
            _ = try _ioctl(&self.dependencyState, self.handle, c.SPI_IOC_RD_MAX_SPEED_HZ, @intFromPtr(&speed));
            self.configuration.speed = speed;
            return self.configuration.speed;
        }

        fn _setSpeed(dependencyState: *DependencyState, handle: c_int, speed: u32) !void {
            _ = try _ioctl(dependencyState, handle, c.SPI_IOC_WR_MAX_SPEED_HZ, @intFromPtr(&speed));
        }

        pub fn setMode(self: *Self, mode: Mode) !void {
            try _setMode(&self.dependencyState, self.handle, mode);
            self.configuration.mode = mode;
        }

        pub fn refreshMode(self: *Self) !Mode {
            var modeBits: u8 = undefined;
            _ = try _ioctl(&self.dependencyState, self.handle, c.SPI_IOC_RD_MODE, @intFromPtr(&modeBits));
            self.configuration.mode = Mode.parseBits(modeBits);
            return self.configuration.mode;
        }

        fn _setMode(dependencyState: *DependencyState, handle: c_int, mode: Mode) !void {
            const modeBits: u8 = mode.toBits();

            _ = try _ioctl(dependencyState, handle, c.SPI_IOC_WR_MODE, @intFromPtr(&modeBits));
        }

        pub fn setBitsPerWord(self: *Self, bitsPerWord: u8) !void {
            try _setBitsPerWord(&self.dependencyState, self.handle, bitsPerWord);
            self.configuration.bitsPerWord = bitsPerWord;
        }

        pub fn refreshBitsPerWord(self: *Self) !u8 {
            var bitsPerWord: u8 = undefined;
            _ = try _ioctl(&self.dependencyState, self.handle, c.SPI_IOC_RD_BITS_PER_WORD, @intFromPtr(&bitsPerWord));
            self.configuration.bitsPerWord = bitsPerWord;
            return self.configuration.bitsPerWord;
        }

        fn _setBitsPerWord(dependencyState: *DependencyState, handle: c_int, bitsPerWord: u8) !void {
            _ = try _ioctl(dependencyState, handle, c.SPI_IOC_WR_BITS_PER_WORD, @intFromPtr(&bitsPerWord));
        }

        pub fn setBitJustification(self: *Self, bitJustification: BitJustification) !void {
            try _setBitJustification(&self.dependencyState, self.handle, bitJustification);
            self.configuration.bitJustification = bitJustification;
        }

        pub fn refreshBitJustification(self: *Self) !BitJustification {
            var isLSBFirst: u8 = undefined;
            _ = try _ioctl(&self.dependencyState, self.handle, c.SPI_IOC_RD_LSB_FIRST, @intFromPtr(&isLSBFirst));
            self.configuration.bitJustification = if (isLSBFirst == 0) .leastSignificantBitFirst else .mostSignificantBitFirst;
            return self.configuration.bitJustification;
        }

        fn _setBitJustification(dependencyState: *DependencyState, handle: c_int, bitJustification: BitJustification) !void {
            const isLSBFirst: u8 = switch (bitJustification) {
                .leastSignificantBitFirst => 1,
                .mostSignificantBitFirst => 0,
            };

            _ = try _ioctl(dependencyState, handle, c.SPI_IOC_WR_LSB_FIRST, @intFromPtr(&isLSBFirst));
        }

        fn _ioctl(dependencyState: *DependencyState, handle: c_int, request: u32, arg: usize) !usize {
            const result = inject(dependencies.ioctl, dependencyState, .{ handle, request, arg });

            const resultAsSignedInt = @bitCast(isize, result);

            if (resultAsSignedInt < 0) {
                return error.OSError;
            }

            return result;
        }
    };
}

pub const SPI = SPIFactory(struct {
    const open = std.os.open;
    const close = std.os.close;
    const ioctl = std.os.linux.ioctl;
});

fn returnType(comptime function: anytype) type {
    return switch (@typeInfo(@TypeOf(function))) {
        .Fn => |functionType| functionType.return_type.?,
        else => @compileError("Tried to get the return type of something that isn't a function"),
    };
}

fn inject(function: anytype, state: anytype, args: anytype) returnType(function) {
    return if (@TypeOf(state) == *void)
        @call(.auto, function, args)
    else
        @call(.auto, function, .{state} ++ args);
}

const OpenTests = struct {
    const IOCTLArg = union(enum) {
        value: usize,
        wr_pointer_u8: u8,
        wr_pointer_i32: i32,
        wr_pointer_u32: u32,
        rd_pointer_u8: u8,
        rd_pointer_i32: i32,
        rd_pointer_u32: u32,

        transfer: []const c.spi_ioc_transfer,
    };

    const TestState = struct {
        open: []const struct { anyerror!std.os.fd_t, []const u8, u32, std.os.mode_t } = &.{},
        close: []const struct { void, std.os.fd_t } = &.{},
        ioctl: []const struct { usize, std.os.fd_t, usize, IOCTLArg } = &.{},

        fn complete(self: TestState) !void {
            try std.testing.expect(self.open.len == 0);
            try std.testing.expect(self.close.len == 0);
            try std.testing.expect(self.ioctl.len == 0);
        }
    };

    const TestDependencies = struct {
        fn open(state: *TestState, devicePath: []const u8, flags: u32, perm: std.os.mode_t) !std.os.fd_t {
            const testCase = state.open[0];
            // Always advance the test case to the next one.
            state.open = state.open[1..state.open.len];

            try std.testing.expectEqualDeep(testCase[1], devicePath);
            try std.testing.expectEqual(testCase[2], flags);
            try std.testing.expectEqual(testCase[3], perm);

            return testCase[0];
        }

        fn close(state: *TestState, fd: std.os.fd_t) void {
            const testCase = state.close[0];
            state.close = state.close[1..state.close.len];
            std.testing.expectEqualDeep(testCase[1], fd) catch unreachable;
        }

        fn ioctl(state: *TestState, fd: std.os.fd_t, request: usize, arg: usize) usize {
            const testCase = state.ioctl[0];
            state.ioctl = state.ioctl[1..state.ioctl.len];

            std.testing.expectEqual(testCase[1], fd) catch unreachable;
            std.testing.expectEqual(testCase[2], request) catch unreachable;
            switch (testCase[3]) {
                .value => |value| std.testing.expectEqual(value, arg) catch unreachable,
                .wr_pointer_u8 => |value| std.testing.expectEqualDeep(&value, @ptrFromInt(*u8, arg)) catch unreachable,
                .wr_pointer_i32 => |value| std.testing.expectEqualDeep(&value, @ptrFromInt(*i32, arg)) catch unreachable,
                .wr_pointer_u32 => |value| std.testing.expectEqualDeep(&value, @ptrFromInt(*u32, arg)) catch unreachable,
                .transfer => |value| {
                    const argSlice = @ptrFromInt([*]c.spi_ioc_transfer, arg)[0..value.len];
                    std.testing.expectEqualDeep(value, argSlice) catch unreachable;
                },

                .rd_pointer_u8 => |value| @ptrFromInt(*u8, arg).* = value,
                .rd_pointer_i32 => |value| @ptrFromInt(*i32, arg).* = value,
                .rd_pointer_u32 => |value| @ptrFromInt(*u32, arg).* = value,
            }

            return testCase[0];
        }

        fn createState() TestState {
            return TestState{};
        }
    };

    const SPITest = SPIFactory(TestDependencies);

    test "open: Validate the path is correct and doesn't this doesn't call close." {
        var spiTest: SPITest = SPITest{ .dependencyState = .{
            .open = &.{
                .{ 0xAA, "/dev/spidev0.0", std.os.O.RDWR | std.os.O.CLOEXEC, 0 },
            },
            .ioctl = &.{
                .{ 0, 0xAA, c.SPI_IOC_WR_MAX_SPEED_HZ, .{
                    .wr_pointer_u32 = 1000 * 1000,
                } },
                .{ 0, 0xAA, c.SPI_IOC_WR_MODE, .{
                    .wr_pointer_u8 = 0,
                } },
                .{ 0, 0xAA, c.SPI_IOC_WR_BITS_PER_WORD, .{
                    .wr_pointer_u8 = 8,
                } },
                .{ 0, 0xAA, c.SPI_IOC_WR_LSB_FIRST, .{
                    .wr_pointer_u8 = 0,
                } },
            },
        } };

        const configuration: SPITest.Configuration = .{};

        try spiTest.open(0, 0, configuration);
        try std.testing.expectEqual(@as(c_int, 0xAA), spiTest.handle);
        try spiTest.dependencyState.complete();
    }

    test "open: An error is properly handled." {
        var spiTest: SPITest = SPITest{ .dependencyState = .{
            .open = &.{
                .{ std.os.OpenError.FileNotFound, "/dev/spidev0.0", std.os.O.RDWR | std.os.O.CLOEXEC, 0 },
            },
        } };

        const configuration: SPITest.Configuration = undefined;

        try std.testing.expectError(
            std.os.OpenError.FileNotFound,
            spiTest.open(0, 0, configuration),
        );
        try std.testing.expectEqual(@as(c_int, -1), spiTest.handle);
        try spiTest.dependencyState.complete();
    }

    test "open: A failed ioctl is handled properly." {
        var spiTest: SPITest = SPITest{ .dependencyState = .{
            .open = &.{
                .{ 0xAA, "/dev/spidev0.0", std.os.O.RDWR | std.os.O.CLOEXEC, 0 },
            },
            .close = &.{
                .{ undefined, 0xAA },
            },
            .ioctl = &.{
                .{ @bitCast(usize, @as(isize, -1)), 0xAA, c.SPI_IOC_WR_MAX_SPEED_HZ, .{
                    .wr_pointer_u32 = 1000 * 1000,
                } },
            },
        } };

        const configuration = SPITest.Configuration{};

        try std.testing.expectError(
            error.OSError,
            spiTest.open(0, 0, configuration),
        );
        try std.testing.expectEqual(@as(c_int, -1), spiTest.handle);
        try spiTest.dependencyState.complete();
    }

    test "open: ioctls are called for all of the fields in the configuration." {
        var spiTest: SPITest = SPITest{ .dependencyState = .{
            .open = &.{
                .{ 0xAA, "/dev/spidev6.7", std.os.O.RDWR | std.os.O.CLOEXEC, 0 },
            },
            .ioctl = &.{
                .{ 0, 0xAA, c.SPI_IOC_WR_MAX_SPEED_HZ, .{
                    .wr_pointer_u32 = 0x12345678,
                } },
                .{ 0, 0xAA, c.SPI_IOC_WR_MODE, .{
                    .wr_pointer_u8 = CLOCK_POLARITY_BIT_MASK,
                } },
                .{ 0, 0xAA, c.SPI_IOC_WR_BITS_PER_WORD, .{
                    .wr_pointer_u8 = 7,
                } },
                .{ 0, 0xAA, c.SPI_IOC_WR_LSB_FIRST, .{
                    .wr_pointer_u8 = 1,
                } },
            },
        } };

        const configuration: SPITest.Configuration = .{
            .speed = 0x12345678,
            .bitsPerWord = 7,
            .mode = .{
                .clockPhase = false,
                .clockPolarity = true,
            },
            .bitJustification = .leastSignificantBitFirst,
        };

        try spiTest.open(6, 7, configuration);
        try std.testing.expectEqual(@as(c_int, 0xAA), spiTest.handle);
        try std.testing.expectEqual(configuration, spiTest.configuration);
        try spiTest.dependencyState.complete();
    }

    test "setters: All update configuration and call the correct IOCTL." {
        var spiTest: SPITest = SPITest{ .handle = 0xCC, .dependencyState = .{
            .ioctl = &.{
                .{ 0, 0xCC, c.SPI_IOC_WR_MAX_SPEED_HZ, .{
                    .wr_pointer_u32 = 0x76543210,
                } },
                .{ 0, 0xCC, c.SPI_IOC_WR_MODE, .{
                    .wr_pointer_u8 = CLOCK_PHASE_BIT_MASK,
                } },
                .{ 0, 0xCC, c.SPI_IOC_WR_BITS_PER_WORD, .{
                    .wr_pointer_u8 = 4,
                } },
                .{ 0, 0xCC, c.SPI_IOC_WR_LSB_FIRST, .{
                    .wr_pointer_u8 = 0,
                } },
            },
        } };

        const configuration = SPITest.Configuration{
            .speed = 0x76543210,
            .bitsPerWord = 4,
            .mode = .{
                .clockPhase = true,
                .clockPolarity = false,
            },
            .bitJustification = .mostSignificantBitFirst,
        };

        try spiTest.setSpeed(configuration.speed);
        try spiTest.setMode(configuration.mode);
        try spiTest.setBitsPerWord(configuration.bitsPerWord);
        try spiTest.setBitJustification(configuration.bitJustification);
        try std.testing.expectEqual(configuration, spiTest.configuration);

        try spiTest.dependencyState.complete();
    }

    test "refresh: All refresh methods pull the values from ioctls" {
        var spiTest: SPITest = SPITest{ .handle = 0xDD, .dependencyState = .{
            .ioctl = &.{
                .{ 0, 0xDD, c.SPI_IOC_RD_MAX_SPEED_HZ, .{
                    .rd_pointer_u32 = 0xA0A0A0,
                } },
                .{ 0, 0xDD, c.SPI_IOC_RD_MODE, .{
                    .rd_pointer_u8 = CLOCK_PHASE_BIT_MASK | CLOCK_POLARITY_BIT_MASK,
                } },
                .{ 0, 0xDD, c.SPI_IOC_RD_BITS_PER_WORD, .{
                    .rd_pointer_u8 = 9,
                } },
                .{ 0, 0xDD, c.SPI_IOC_RD_LSB_FIRST, .{
                    .rd_pointer_u8 = 1,
                } },
            },
        } };

        try std.testing.expectEqual(@as(u32, 0xA0A0A0), try spiTest.refreshSpeed());
        try std.testing.expectEqual(SPITest.Mode{ .clockPhase = true, .clockPolarity = true }, try spiTest.refreshMode());
        try std.testing.expectEqual(@as(u8, 9), try spiTest.refreshBitsPerWord());
        try std.testing.expectEqual(SPITest.BitJustification.mostSignificantBitFirst, try spiTest.refreshBitJustification());

        const configuration = SPITest.Configuration{
            .speed = 0xA0A0A0,
            .bitJustification = .mostSignificantBitFirst,
            .bitsPerWord = 9,
            .mode = .{
                .clockPhase = true,
                .clockPolarity = true,
            },
        };

        try std.testing.expectEqual(configuration, spiTest.configuration);
        try spiTest.dependencyState.complete();
    }

    test "transfer: Ignore empty transfers" {
        var spiTest: SPITest = SPITest{ .handle = 0xDD, .dependencyState = .{} };

        try std.testing.expectEqual(@as(usize, 0), try spiTest.transfer(&.{}, &.{}));
        try spiTest.dependencyState.complete();
    }

    test "transfer: A single Read sets up a valid structure." {
        var buffer: [7]u8 = undefined;
        const configuration = SPITest.Configuration{};

        var spiTest: SPITest = SPITest{
            .handle = 0xEE,
            .configuration = configuration,
            .dependencyState = .{
                .ioctl = &.{
                    .{
                        7, 0xEE, try spiIOCRequest(1), .{
                            .transfer = &[_]c.spi_ioc_transfer{
                                std.mem.zeroInit(c.spi_ioc_transfer, .{
                                    .tx_buf = 0,
                                    .rx_buf = @intFromPtr(&buffer),
                                    .len = 7,
                                    .speed_hz = configuration.speed,
                                    .bits_per_word = configuration.bitsPerWord,
                                }),
                            },
                        },
                    },
                },
            },
        };

        var transfers = [_]SPITest.Transfer{
            .{ .data = .{ .receive = &buffer } },
        };

        var messages: [transfers.len]c.spi_ioc_transfer = undefined;

        try std.testing.expectEqual(@as(usize, 7), try spiTest.transfer(&transfers, &messages));

        try spiTest.dependencyState.complete();
    }

    test "transfer: Complex transfers setup a valid structure." {
        var transmit1: [4]u8 = undefined;
        var receive2: [5]u8 = undefined;
        var transmit3: [6]u8 = undefined;
        var receive3: [6]u8 = undefined;

        const configuration = SPITest.Configuration{};

        var spiTest: SPITest = SPITest{
            .handle = 0xFEF,
            .configuration = configuration,
            .dependencyState = .{
                .ioctl = &.{
                    .{
                        7, 0xFEF, try spiIOCRequest(3), .{
                            .transfer = &[_]c.spi_ioc_transfer{
                                std.mem.zeroInit(c.spi_ioc_transfer, .{
                                    .tx_buf = @intFromPtr(&transmit1),
                                    .rx_buf = 0,
                                    .len = transmit1.len,
                                    .speed_hz = configuration.speed,
                                    .bits_per_word = configuration.bitsPerWord,
                                }),
                                std.mem.zeroInit(c.spi_ioc_transfer, .{
                                    .tx_buf = 0,
                                    .rx_buf = @intFromPtr(&receive2),
                                    .len = receive2.len,
                                    .speed_hz = configuration.speed,
                                    .bits_per_word = configuration.bitsPerWord,
                                    .cs_change = 1,
                                }),
                                std.mem.zeroInit(c.spi_ioc_transfer, .{
                                    .tx_buf = @intFromPtr(&transmit3),
                                    .rx_buf = @intFromPtr(&receive3),
                                    .len = transmit3.len,
                                    .speed_hz = configuration.speed,
                                    .bits_per_word = configuration.bitsPerWord,
                                }),
                            },
                        },
                    },
                },
            },
        };

        var transfers = [_]SPITest.Transfer{
            .{ .data = .{ .transmit = &transmit1 } },
            .{ .data = .{ .receive = &receive2 }, .deselectAfterTransfer = true },
            .{ .data = .{ .duplex = .{ .transmit = &transmit3, .receive = &receive3 } } },
        };

        var messages: [transfers.len]c.spi_ioc_transfer = undefined;

        try std.testing.expectEqual(@as(usize, 7), try spiTest.transfer(&transfers, &messages));

        try spiTest.dependencyState.complete();
    }
};

fn spiMessageSize(numMessages: usize) !u32 {
    const sizeBytes = numMessages * @sizeOf(c.spi_ioc_transfer);
    if (sizeBytes > 1 << c._IOC_SIZEBITS) {
        return error.InvalidArgument;
    }

    return @truncate(u32, sizeBytes);
}

fn spiIOCRequest(numMessages: usize) !u32 {
    const messageSize = try spiMessageSize(numMessages);
    return c._IOC_WRITE << c._IOC_DIRSHIFT |
        c.SPI_IOC_MAGIC << c._IOC_TYPESHIFT |
        messageSize << c._IOC_SIZESHIFT;
}

test {
    std.testing.refAllDeclsRecursive(OpenTests);

    // Some test code to see what the value of the ioctl returns on error.
    // Key here was to use bitCast to convert from the usize to a negative
    // number.
    // const bitsPerWord = 10;
    // const result = std.os.linux.ioctl(0xFACE, c.SPI_IOC_WR_BITS_PER_WORD, @intFromPtr(&bitsPerWord));
    // std.debug.print("Ran the IOCTL({d}), result({d})\n", .{ c.SPI_IOC_WR_BITS_PER_WORD, result });

    // var spiTest: SPI = undefined;
    // const configuration: SPI.Configuration = undefined;

    // try spiTest.open(0, 0, configuration);
}
