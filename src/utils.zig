const std = @import("std");

pub fn ArrayWithSlice(comptime T: type, comptime size: usize) type {
    return struct {
        buffer: [size]T,
        slice: []T,

        const Self = @This();

        pub fn init(self: *Self, buffer: [size]T, end: usize) void {
            std.mem.copy(u8, &self.buffer, &buffer);

            self.slice = self.buffer[0..end];
        }

        pub fn format(
            self: Self,
            comptime fmt: []const u8,
            options: std.fmt.FormatOptions,
            writer: anytype,
        ) !void {
            _ = options;
            _ = fmt;

            return std.fmt.format(writer, "{s}", .{self.slice});
        }
    };
}

pub fn expandArray(comptime T: type, comptime maxLength: usize, value: []const T) [maxLength]T {
    var result: [maxLength]T = undefined;
    @memset(&result, 0);
    const toCopy = if (value.len >= maxLength) maxLength else value.len;
    std.mem.copy(T, &result, value[0..toCopy]);

    return result;
}
